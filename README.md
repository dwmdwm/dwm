# dwm - dynamic window manager

[![dwm](https://gitlab.com/dwmdwm/dwm/raw/main/dwm.png)](https://gitlab.com/dwmdwm/dwm)

### Get started
dwm is a dynamic window manager for X. It manages windows in tiled, monocle and floating layouts. All of the layouts can be applied dynamically, optimising the environment for the application in use and the task performed.

### Download and install
```bash
git clone --depth 1 https://gitlab.com/dwmdwm/dwm $HOME/.dwm
cd $HOME/.dwm
sudo make clean install
```

### Fonts
```bash
sudo pacman -S noto-fonts noto-fonts-cjk noto-fonts-emoji terminus-font powerline-fonts ttf-nerd-fonts-symbols
```

### Typeface
List of Computer font
```
Segoe UI
Roboto
Helvetica Neue
Arial
Noto Sans
Liberation Sans
sans-serif
```

List of Monospaced font
```
Hack
Fira Code
Source Code Pro
Source Code Variable
Cascadia Code
Cascadia Mono
JetBrains Mono
Terminus
SFMono-Regular
Menlo
Monaco
Consolas
Liberation Mono
Courier New
Noto Sans Mono
WenQuanYi Micro Hei Mono
Microsoft YaHei Mono
monospace
```

List of Symbols font
```
Noto Sans Symbols
PowerlineSymbols
Symbols Nerd Font
```

List of Emoji font
```
Apple Color Emoji
Segoe UI Emoji
Noto Color Emoji
```
